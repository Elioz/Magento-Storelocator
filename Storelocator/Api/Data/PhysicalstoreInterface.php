<?php
namespace Esgi\Storelocator\Api\Data;

/**
 * Esgi physicalstore interface.
 * @api
 */
interface PhysicalstoreInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ID      = 'entity_id';
    const NAME    = 'name';
    const ADDRESS = 'address';
    const CITY = 'city';
    const POSTAL_CODE = 'postal_code';
    const PHONE = 'phone';
    const FAX = 'fax';
    const SCHEDULE = 'schedule';
    const LATITUDE = 'latitude';
    const LONGITUDE = 'longitude';
    /**#@-*/

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get name
     *
     * @return string|null
     */
    public function getName();

    /**
     * Get address
     *
     * @return string|null
     */
    public function getAddress();

    /**
     * Get city
     *
     * @return string|null
     */
    public function getCity();

    /**
     * Get Postal code
     *
     * @return string|null
     */
    public function getPostalCode();

    /**
     * Get phone
     *
     * @return string|null
     */
    public function getPhone();

    /**
     * Get fax
     *
     * @return string|null
     */
    public function getFax();

    /**
     * Get schedule
     *
     * @return string|null
     */
    public function getSchedule();

    /**
     * Get latitude
     *
     * @return string|null
     */
    public function getLatitude();

    /**
     * Get longitude
     *
     * @return string|null
     */
    public function getLongitude();

    /**
     * Set ID
     *
     * @param int $id
     * @return PhysicalstoreInterface
     */
    public function setId($id);

    /**
     * Set name
     *
     * @param string $name
     * @return PhysicalstoreInterface
     */
    public function setName($name);

    /**
     * Set address
     *
     * @param string $address
     * @return PhysicalstoreInterface
     */
    public function setAddress($address);

    /**
     * Set city
     *
     * @param int $city
     * @return PhysicalstoreInterface
     */
    public function setCity($city);

    /**
     * Set postal code
     *
     * @param string $postalCode
     * @return PhysicalstoreInterface
     */
    public function setPostalCode($postalCode);

    /**
     * Set phone
     *
     * @param string $phone
     * @return PhysicalstoreInterface
     */
    public function setPhone($phone);

    /**
     * Set fax
     *
     * @param string $fax
     * @return PhysicalstoreInterface
     */
    public function setFax($fax);

    /**
     * Set schedule
     *
     * @param string $schedule
     * @return PhysicalstoreInterface
     */
    public function setSchedule($schedule);

    /**
     * Set latitude
     *
     * @param string $latitude
     * @return PhysicalstoreInterface
     */
    public function setLatitude($latitude);

    /**
     * Set longitude
     *
     * @param string $longitude
     * @return PhysicalstoreInterface
     */
    public function setLongitude($longitude);
}
