<?php
namespace Esgi\Storelocator\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Esgi job CRUD interface.
 * @api
 */
interface PhysicalstoreRepositoryInterface
{
    /**
     * Save block.
     *
     * @param \Esgi\Storelocator\Api\Data\PhysicalstoreInterface $physicalstore
     * @return \Esgi\Storelocator\Api\Data\PhysicalstoreInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(Data\PhysicalstoreInterface $physicalstore);

    /**
     * Retrieve $physicalstore.
     *
     * @param int $physicalstoreId
     * @return \Esgi\Storelocator\Api\Data\PhysicalstoreInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($physicalstoreId);

    /**
     * Retrieve physicalstores matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Esgi\Storelocator\Api\Data\PhysicalstoreSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete physicalstore.
     *
     * @param \Esgi\Storelocator\Api\Data\PhysicalstoreInterface $physicalstore
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(Data\PhysicalstoreInterface $physicalstore);

    /**
     * Delete physicalstore by ID.
     *
     * @param int $physicalstoreId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($physicalstoreId);
}
