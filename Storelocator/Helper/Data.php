<?php

namespace Esgi\Storelocator\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{

    const XML_PATH_ESGI_TAX_AMOUNT = 'egsi/general/tax_amount';

    /**
     * @param $field
     * @param null $storeId
     * @return mixed
     */
    public function getConfigValue($field, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $field, ScopeInterface::SCOPE_STORE, $storeId
        );
    }

    /**
     * @param null $storeId
     * @return mixed
     */
    public function getEsgiTaxAmount($storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_ESGI_TAX_AMOUNT , $storeId);
    }

}