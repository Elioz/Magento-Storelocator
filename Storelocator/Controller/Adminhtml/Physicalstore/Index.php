<?php
namespace Esgi\Storelocator\Controller\Adminhtml\Physicalstore;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action;

class Index extends Action
{
    const ADMIN_RESOURCE = 'Esgi_Storelocator::physicalstore';

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Esgi_Storelocator::physicalstore');
        $resultPage->addBreadcrumb(__('Storelocators'), __('Storelocators'));
        $resultPage->addBreadcrumb(__('Manage Physical Stores'), __('Manage Physical Stores'));
        $resultPage->getConfig()->getTitle()->prepend(__('Physical Store'));

        return $resultPage;
    }
}
