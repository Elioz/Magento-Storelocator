<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Esgi\Storelocator\Block\Adminhtml\Physicalstore\Edit;

use Magento\Backend\Block\Widget\Context;
use Esgi\Storelocator\Api\PhysicalstoreRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class GenericButton
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var PhysicalstoreRepositoryInterface
     */
    protected $physicalstoreRepository;

    /**
     * @param Context $context
     * @param PhysicalstoreRepositoryInterface $physicalstoreRepository
     */
    public function __construct(
        Context $context,
        PhysicalstoreRepositoryInterface $physicalstoreRepository
    ) {
        $this->context              = $context;
        $this->physicalstoreRepository = $physicalstoreRepository;
    }

    /**
     * Return Storelocator physicalstore ID
     *
     * @return int|null
     */
    public function getPhysicalstoreId()
    {
        try {
            return $this->physicalstoreRepository->getById(
                $this->context->getRequest()->getParam('id')
            )->getId();
        } catch (NoSuchEntityException $e) {
        }
        return null;
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
