<?php
// app/code/Esgi/Storelocator/Block/Physicalstore.php
namespace Esgi\Storelocator\Block;

use Magento\Framework\View\Element\Template;
use Esgi\Storelocator\Api\PhysicalstoreRepositoryInterface;
use Esgi\Storelocator\Model\ResourceModel\Physicalstore\Collection;

/**
 * Physicalstore block
 */
class Physicalstore extends \Magento\Framework\View\Element\Template
{
    protected $_collection;

    public function __construct(
        Collection $collection,
        Template\Context $context,
        array $data = []
    ) {
        $this->_collection = $collection;
        parent::__construct($context, $data);
    }

    public function getPhysicalstores()
    {
        return $this->_collection->getItems();
    }
}
