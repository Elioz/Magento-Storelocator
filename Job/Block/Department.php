<?php
// app/code/Esgi/Job/Block/Department.php
namespace Esgi\Job\Block;

use Magento\Framework\View\Element\Template;
use Esgi\Job\Api\DepartmentRepositoryInterface;
use Esgi\Job\Model\ResourceModel\Department\Collection;

/**
 * Department block
 */
class Department extends \Magento\Framework\View\Element\Template
{
    protected $_collection;

    public function __construct(
        Collection $collection,
        Template\Context $context,
        array $data = []
    ) {
        $this->_collection = $collection;
        parent::__construct($context, $data);
    }

    public function getDepartments()
    {
        return $this->_collection->getItems();
    }
}
